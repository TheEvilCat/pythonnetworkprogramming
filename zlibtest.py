#compress - decompress
import zlib

data = zlib.compress(b'luis') + b'.' + zlib.compress(b'kr') + b'.'
print("compressed data is", data)
print("length of data is:", len(data))
d = zlib.decompressobj()
print("d is:", d)

print("afer decompress: ", d.decompress(data[0:48]), d.unused_data)